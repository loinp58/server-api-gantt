<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Requirements
- PHP >= 5.6.4
- composer
- Mysql


## Installation
Clone this project:
```
git clone git@gitlab.com:loinp58/server-api-gantt.git
```

Install dependencies:
```
composer install
```

Configure local environment:
```
cp .env.example .env
php artisan key:generate
```

Change username, password, database in `.env` file. Example:
```
DB_DATABASE=server
DB_USERNAME=root
DB_PASSWORD=
```
Migrate database:
```
php artisan migrate
```
Runs the server in the development mode
```
php artisan serve
```
Project started [http://localhost:8000](http://localhost:8000)