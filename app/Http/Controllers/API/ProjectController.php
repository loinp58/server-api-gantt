<?php

namespace App\Http\Controllers\API;

use App\Model\Task;
use App\Model\Link;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProjectController extends Controller
{
    public function index()
    {
        $tasks = Task::all();
        $result = array();
        foreach ($tasks as $index => $task) {
            $result[$index] = array(
                'id' => $task->id,
                'text' => $task->text,
                'type' => $this->getTypeText($task->type),
                'progress' => $task->progress,
                'open' => true,
            );
            if ($task->start_date != null) {
                $result[$index]['start_date'] = date('d-m-Y', strtotime($task->start_date));
            }
            if ($task->type == 0) {
                $result[$index]['duration'] = $task->duration;
            }
            if ($task->parent != 0) {
                $result[$index]['parent'] = $task->parent;
            }
        }
        $links = Link::all();
        $dataLink = array();
        foreach ($links as $link) {
            $dataLink[] = array(
                'id' => $link->id,
                'source' => $link->source,
                'target' => $link->target,
                'type' => $link->type
            );
        }
        return response()->json(array('data' => $result, 'links' => $dataLink));
    }

    private function getTypeText($type)
    {
        switch ($type) {
            case 0:
                $result = 'task';
                break;
            case 1:
                $result = 'project';
                break;
            default:
                $result = 'milestone';
                break;
        }
        return $result;
    }
}
