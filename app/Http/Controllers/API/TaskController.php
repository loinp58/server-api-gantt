<?php

namespace App\Http\Controllers\API;

use App\Model\Task;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $task = new Task;
        $task->text = $data['text'];
        $task->type = isset($data['type']) ? config('system.type')[$data['type']] : 0;
        $task->start_date = $data['start_date'];
        $task->duration = isset($data['duration']) ? intval($data['duration']) : 0;
        $task->progress = isset($data['progress']) ? doubleval($data['progress']) : 0;
        $task->parent = isset($data['parent']) ? intval($data['parent']) : 0;
        $task->save();
        return response()->json([
            "action"=> "inserted",
            "tid" => $task->id
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $task = Task::find($id);
        $task->text = $data['text'];
        $task->type = isset($data['type']) ? config('system.type')[$data['type']] : 0;
        $task->start_date = $data['start_date'];
        $task->duration = isset($data['duration']) ? intval($data['duration']) : 0;
        $task->progress = isset($data['progress']) ? doubleval($data['progress']) : 0;
        $task->parent = isset($data['parent']) ? intval($data['parent']) : 0;
        $task->save();
        return response()->json([
            "action"=> "updated"
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Task::destroy($id);
            $childTasks = Task::where('parent', $id);
            foreach ($childTasks as $childTask) {
                $childTask->delete();
            }
            return response()->json([
                "action"=> "deleted"
            ]);
        }
        catch (\Exception $e) {
            return response()->json(array('result' => false));
        }
    }
}
